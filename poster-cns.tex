% !TEX encoding = UTF-8
% !TEX spellcheck = en_US
\documentclass[final]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usetheme{RJHlandscapeUniSA}
\usepackage[orientation=landscape,size=a0,scale=1.4]{beamerposter}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,decorations.markings,positioning}
\tikzset{>=stealth}

\usepackage[absolute,overlay]{textpos}
\usepackage[numbers,compress]{natbib}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{array}
%\usepackage{wrapfig}
\usepackage{rotating}

\graphicspath{{figures/}}

\newcolumntype{C}[1]{>{\centering\arraybackslash} m{#1} }
\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}
\newcommand{\subblock}[1]{\bigskip\textbf{#1}}

\title{Biphasic ERP responses are not just filter artifacts }
\author{Phillip M. Alday  \and Sabine Frenzel \and Matthias Schlesewsky \and Ina Bornkessel-Schlesewsky}
\institute{University of South Australia}
\footer{}
%\footer{Live code at \url{https://github.com/jona-sassenhagen/phijona---gamma}}
\date{}

\newcommand{\tikzmark}[3][]{\tikz[overlay,remember picture,baseline] \node [anchor=base,#1](#2) {#3};}
\DeclareMathOperator*{\cooc}{cooc}
\DeclareMathOperator*{\sigcooc}{sig}
% color blind safe colours, see http://www.cookbook-r.com/Graphs/Colors_%28ggplot2%29/#a-colorblind-friendly-palette
\definecolor{lightorange}{HTML}{E69F00}
\definecolor{lightblue}{HTML}{56B4E9}
\definecolor{lightgreen}{HTML}{009E73}
\definecolor{lightyellow}{HTML}{F0E442}
\definecolor{darkblue}{HTML}{0072B2}
\definecolor{darkorange}{HTML}{D55E00}
\definecolor{darkpink}{HTML}{CC79A7}

\begin{document}
\begin{frame}{} 	


\begin{textblock}{48}(1,7)

\begin{block}{Introduction}
Filtering is one of the most basic yet most poorly understood steps in a typical ERP analysis.
Filters fundamentally change the data in ways beyond just the desirable, a fundamental fact which has seen increasing attention and debate in the literature.\cite{luck2005a,vanrullen2011a,rousselet2012a,widmannschroger2012a,acunzomackenzierossum2012a}
Recently, Tanner et al.\ have demonstrated that high-pass filtering can introduce spurious components before a real effect, e.g. an N400 before a P600.\cite{tanner.morgan-short.etal:2015p}
This raises the question whether the different component patterns observed across languages for the same manipulation are an artifact of different filtering practices. 
\end{block}

\begin{block}{Describing and classifying filter response}
Filter distortions are related to two fundamental properties of filtering: filters remove parts of the signal and there is no such thing as a digital filter which removes only the frequencies that we want. 

\begin{tabular}{m{0.5\textwidth} m{0.5\textwidth} }
\includegraphics[width=0.48\textwidth, trim=0 5.9cm 0 0,clip=true]{rousselet2012-fig01.pdf} & \includegraphics[width=0.48\textwidth, trim=0 0 0 9cm,clip=true]{rousselet2012-fig01.pdf} \\
\end{tabular}


\begin{description}
% \item[Frequency magnitude response] a frequency-domain description of how a filter interacts with input signals
\item[Phase response] the difference in phase, at a particular frequency, between an input sine wave and the output sine wave at that frequency. 
\item[Impulse response] a digital filter's time-domain output sequence when the input is a single unity-valued sample (\textbf{impulse}) preceded and followed by zero-valued samples.
\end{description}
\hfill \tiny images reproduced from \cite{rousselet2012a}, definitions adapted from \cite{lyons:2011}

\end{block}

%\begin{block}{Impulse response: ripple effects in filtering}
%\end{block}

\begin{block}{Phase response: time-shifting in filtering}
Filtering impacts both the amplitude and temporal relationships of constituent frequencies.

\begin{description}
%\item[Linear-phase filters] exhibit a constant change in phase angle as a function of frequency, which implies that the group delay is constant.
\item[Group delay] can be thought of as the propagation time delay of the information of an amplitude-modulated signal (e.g. ERP) passing through a digital filter. Non-constant passband group delay leads to distortion occurs because different frequencies take different amounts of time to pass through the filter.
\item[Causal filters] are computed only from past input and necessarily introduce group delay, thus distorting component latencies.
\item[Non-causal filters] are computed (offline) from both past and future input and can achieve zero-phase (and thus zero group delay) at the cost of smearing filter artifacts forward and backward in time.
\end{description}
\vspace{-0.75cm}\hfill {\tiny adapted in part from \cite{lyons:2011}, cf. \cite{rousselet2012a,widmannschroger2012a,widmann.schroger.etal:2015jnm}}
%\includegraphics[width=\textwidth]{rousselet2012-figA01.pdf} 
%\includegraphics[width=\textwidth]{acunzo2012-fig01.pdf} 
\end{block}


\begin{block}{Infinite vs. Finite Impulse Response in filtering} 
\centering
\begin{tabular}{p{19cm} l l}
Characteristic & IIR & FIR (non-recursive) \\
\midrule
Number of necessary multiplications & Least & Most \\
%Sensitivity to filter coefficient quantization & Can be high & Very low \\
% Probability of overflow errors & Can be high & Very low \\
Stability & Must be designed  & Guaranteed \\
%Linear phase & No & Guaranteed {\footnotesize with (anti)symmetrical coefficients}   \\
Can simulate prototype analog filters & Yes & No \\
Required coefficient memory & Least & Most \\
%Hardware filter control complexity & Moderate & Simple \\
%Availability of design software & Good & Very good \\
%Ease of design, or complexity of design software & Moderately complicated & Simple \\
%Difficulty of quantization noise analysis & Most complicated & Least complicated \\
% Supports adaptive filtering & With difficulty & Yes \\
%Methods for achieving zero-phase & forward-backward filtering & forward-backward filtering \\ 
%& & left-shift {\footnotesize with (anti)symmetrical coefficients} \\
\bottomrule
& & \hfill {\tiny adapted from \cite{lyons:2011}} \\
\end{tabular} 
%IIR filters can be computationally simpler than FIR, and especially the Butterworth IIR has a close to ideal frequency response. However, the \emph{infinite} portion of the impulse response as well as the necessity of forward-backing filtering to achieve zero-phase

Although offering potential computational advantages, the potentially \emph{infinitely long} artifacts as well as the necessity of forward-backward filtering to achieve zero-phase are disadvantages of IIR filters.
\end{block}


\end{textblock}

\begin{textblock}{53}(50.5,7)
\begin{block}{Data}
Data were reanalysed from \cite{roehmsoracebornkessel-schlesewsky2013a}. For simplicity of presentation and analysis, we restricted ourselves to the \emph{motion} contrast and the posterior electrodes, where both N400 and the P600 are canonically strongest. The originally reported effect was a biphasic N400-P600 response for the violation condition.
\end{block}

\begin{block}{Beyond the passband: impulse-response type makes a difference}
Causal filters have been proposed as a solution to the types of distortions observed by \cite{tanner.morgan-short.etal:2015p} at the cost of distorting (absolute) component latencies.\cite{rousselet2012a,widmannschroger2012a,widmann.schroger.etal:2015jnm}
While this seems a viable option for less peaky endogenous components such as the N400, many popular analysis packages do not offer a causal filter option.
Here, we focused on the effect of impulse-response type and the lower passband edge in artifacts in non-causal filtering. 


\begin{tabular}{p{2cm} c c c}
& 0.16 Hz & 0.2 Hz & 0.3 Hz \\
{\vspace{-6cm} FIR} & \includegraphics[width=0.3\textwidth]{erpcmp_motion_fft-016_30_200ms_baseline.pdf} &% 
\includegraphics[width=0.3\textwidth]{erpcmp_motion_fft-02_30_200ms_baseline.pdf} &%
\includegraphics[width=0.3\textwidth]{erpcmp_motion_fft-03_30_200ms_baseline.pdf} \\
{\vspace{-6cm} IIR} & \includegraphics[width=0.3\textwidth]{erpcmp_motion_iir-016_30_200ms_baseline.pdf} &% 
\includegraphics[width=0.3\textwidth]{erpcmp_motion_iir-02_30_200ms_baseline.pdf} &%
\includegraphics[width=0.3\textwidth]{erpcmp_motion_iir-03_30_200ms_baseline.pdf} \\

\end{tabular}

Using MNE in Python\cite{mne}, we examined the effect of impulse-response type (\textbf{FIR vs. IIR}) and lower passband edge (\textbf{0.16 vs. 0.2 vs. 0.3 Hz}), while holding the upper passband edge constant at 30 Hz and using a 200ms pre-stimulus baseline correction. Zero-phase was achieved by forward-backward filtering.

%\cite{rousselet2012a,widmannschroger2012a,widmann.schroger.etal:2015jnm}
\end{block}

\begin{block}{Hierarchical modeling of filter and experimental effects}
\begin{tabular}{c p{1cm} c}
\begin{minipage}{0.4\textwidth}
To examine the interaction between filter and experimental manipulation, we used linear mixed- effects models \cite{bates.maechler.etal:2015,fox.weisberg:2011} to model all effects simultaneously in the N400 window (see also \cite{gelmanstern2006a}).

\begin{itemize}
\item The main effect for experimental manipulation remains significant.
\item There is a main effect for passband.
\item There is a main effect for impulse-response type.
\item \alert{There is no interaction between impulse-response type, passband and experimental manipulation}, either pairwise or all together.
\end{itemize}
\end{minipage}
%
& &
%
\begin{minipage}{0.5\textwidth}
\begin{tabular}{lrrr}
Effect & $\chi^2$ & df & $\textrm{Pr}\left(>\chi^2\right)$ \\ 
\midrule
  lateral & 16.98 & 2 & < 0.001 \\ 
  manipulation & 497.57 & 1 & < 0.001 \\ 
  response type & 5.34 & 1 & 0.0209 \\ 
  passband & 13.04 & 2 & 0.0015 \\ 
  lateral:manipulation & 71.66 & 2 & < 0.001 \\ 
  lateral:response type & 1.27 & 2 & 0.5307 \\ 
  manipulation:response type & 0.01 & 1 & 0.9084 \\ 
  lateral:passband & 1.38 & 4 & 0.8473 \\ 
  manipulation:passband & 0.63 & 2 & 0.7302 \\ 
  response type:passband & 2.62 & 2 & 0.2695 \\ 
%  lateral:manipulation:response type & 0.05 & 2 & 0.9765 \\ 
%  lateral:manipulation:passband & 1.33 & 4 & 0.8564 \\ 
%  lateral:response type:passband & 1.89 & 4 & 0.7565 \\ 
  manipulation:response type:passband & 0.72 & 2 & 0.6983 \\ 
%  lateral:manipulation:response type:passband & 0.07 & 4 & 0.9994 \\ 
\bottomrule
\end{tabular}
\end{minipage}
\end{tabular}

\vspace{2.05cm}
\end{block}

\end{textblock}

\begin{textblock}{13}(105,7)
\begin{block}{Conclusion}
%Filtering changes the data in complex ways.
%
%\bigskip
The selection of an appropriate filter must be an informed choice for the experimental design in question.

\bigskip
There is no one-size-fits all in convolution (filtering, baseline correction, averaging, etc.).\cite{lyons:2011,maess.schroger.etal:2016jnm,maess.schroger.etal:2016jnma}

\bigskip 
There are biphasic ERP responses independent of filter effects.

\bigskip
The different ERP patterns observed across languages cannot be reduced to differences in filtering practices across laboratories.

%\begin{tabular}{p{0.70\textwidth}}
%lorem ipsum
%\end{tabular}
\end{block}

\begin{block}{Literature}
\tiny
\bibliographystyle{poster}
\bibliography{cns.bib}
\vspace{0.25cm}
\end{block}

\begin{block}{Get a copy!}
\centering
\includegraphics[width=0.95\textwidth]{poster-cns-url.eps}
\end{block}
\end{textblock}

\end{frame}
\end{document}
