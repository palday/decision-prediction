#!/usr/bin/python
# -*- coding: utf8 -*-

import pylab
import matplotlib.pyplot as plt
import mne
import numpy as np
import os.path
import seaborn as sns

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay")
montage = mne.channels.read_montage(kind="standard_1020")

try:
    import cPickle as pickle
except ImportError:
    import pickle

for lb in (0.16, 0.2, 0.3):
    for hb in (30,):
        for ir in ('iir','fft'):
            print "{}: {} to {} Hz".format(ir.upper(),lb,hb)

            for bl in (True,False):
                #bl = True
                #lb, hb, ir = 0.3, 30, 'fft'
                if bl:
                    baseline = -0.2,0
                    blstr = "_200ms_baseline"
                    print "Baseline: 200ms prestimulus"
                else:
                    baseline = None
                    blstr = ""
                    print "Baseline: none"
                filtstr = "{}-{}_{}{}".format(ir,lb,hb,blstr)

                with open("{}-cache.pickle".format(filtstr),"rb") as pfile:
                    evoked = pickle.load(pfile)
                    evoked_by_cond = pickle.load(pfile)
                    grand_average = pickle.load(pfile)

                    #chans = ["F3","Fz","F4","CP5","Cz","CP6","P3","Pz","P6"]
                    chans = ["Cz","CPz","Pz"]
                    
                    motion_hat = grand_average['A-hat'].pick_channels(chans,copy=True)
                    motion_hat.comment = "haben (n = 34)"
                    motion_ist = grand_average['A-ist'].pick_channels(chans,copy=True)
                    motion_ist.comment = "sein (n = 34)"

                    state_hat = grand_average['B-hat'].pick_channels(chans,copy=True)
                    state_hat.comment = "haben (n = 34)"
                    state_ist = grand_average['B-ist'].pick_channels(chans,copy=True)
                    state_ist.comment = "sein (n = 34)"


                    prefix_hat = grand_average['C-hat'].pick_channels(chans,copy=True)
                    prefix_hat.comment = "haben (n = 34)"
                    prefix_ist = grand_average['C-ist'].pick_channels(chans,copy=True)
                    prefix_ist.comment = "sein (n = 34)"

                    intrans_hat = grand_average['D-hat'].pick_channels(chans,copy=True)
                    intrans_hat.comment = "haben (n = 34)"
                    intrans_ist = grand_average['D-ist'].pick_channels(chans,copy=True)
                    intrans_ist.comment = "sein (n = 34)"
                    
                    motion  = motion_hat - motion_ist
                    state   = state_hat - state_ist
                    prefix  = prefix_hat - prefix_ist
                    intrans = intrans_ist - intrans_hat

                    #motion  = [motion_hat, motion_ist]
                    #state   = [state_hat, state_ist]
                    #prefix  = [prefix_hat, prefix_ist]
                    #intrans = [intrans_ist,intrans_hat]

                    #colors = ["red","blue"]
                    #yscale = dict(eeg=[5, -5])
                    yscale = None

                    #plt_motion = mne.viz.plot_evoked_topo(motion,color=colors,ylim=yscale,show=False)
                    plt_motion = motion.plot(hline=[0],ylim=yscale,show=False)
                    plt_motion.get_axes()[0].set_ylabel(u"µV")
                    plt_motion.get_axes()[0].set_xlabel(u"Time (ms)")
                    plt_motion.get_axes()[0].set_title("Motion: haben > sein")
                    plt_motion.savefig(os.path.join("figures","erpdiff_motion_{}.pdf".format(filtstr.replace('.',''))))

                    plt_state =  state.plot(hline=[0],ylim=yscale,show=False)
                    plt_state.get_axes()[0].set_ylabel(u"µV")
                    plt_state.get_axes()[0].set_xlabel(u"Time (ms)")
                    plt_state.get_axes()[0].set_title("State change: haben > sein")
                    plt_state.savefig(os.path.join("figures","erpdiff_state_{}.pdf".format(filtstr.replace('.',''))))

                    plt_prefix =  prefix.plot(hline=[0],ylim=yscale,show=False)
                    plt_prefix.get_axes()[0].set_ylabel(u"µV")
                    plt_prefix.get_axes()[0].set_xlabel(u"Time (ms)")
                    plt_prefix.get_axes()[0].set_title("Prefixed state change: haben > sein")
                    plt_prefix.savefig(os.path.join("figures","erpdiff_prefix_{}.pdf".format(filtstr.replace('.',''))))

                    plt_intrans =  intrans.plot(hline=[0],ylim=yscale,show=False)
                    plt_intrans.get_axes()[0].set_ylabel(u"µV")
                    plt_intrans.get_axes()[0].set_xlabel(u"Time (ms)")
                    plt_intrans.get_axes()[0].set_title("Other intransitive: sein > haben")
                    plt_intrans.savefig(os.path.join("figures","erpdiff_intrans_{}.pdf".format(filtstr.replace('.',''))))

                    chans = ["CPz"]
                    motion_hat = motion_hat.pick_channels(chans,copy=True)
                    motion_ist = motion_ist.pick_channels(chans,copy=True)
                    state_hat = state_hat.pick_channels(chans,copy=True)
                    state_ist = state_ist.pick_channels(chans,copy=True)
                    prefix_hat = prefix_hat.pick_channels(chans,copy=True)
                    prefix_ist = prefix_ist.pick_channels(chans,copy=True)
                    intrans_hat = intrans_hat.pick_channels(chans,copy=True)
                    intrans_ist = intrans_ist.pick_channels(chans,copy=True)


                    plt_motion = mne.viz.plot_evoked(motion_ist,hline=[0],ylim=yscale,show=False)
                    plt_motion = mne.viz.plot_evoked(motion_hat,axes=plt_motion.get_axes(),show=False)
                    plt_motion.get_axes()[0].set_ylabel(u"µV")
                    plt_motion.get_axes()[0].set_xlabel(u"Time (ms)")
                    plt_motion.get_axes()[0].set_title("Motion: haben > sein")
                    plt_motion.savefig(os.path.join("figures","erpcmp_motion_{}.pdf".format(filtstr.replace('.',''))))

                    plt_state = mne.viz.plot_evoked(state_ist,hline=[0],ylim=yscale,show=False)
                    plt_state = mne.viz.plot_evoked(state_hat,axes=plt_motion.get_axes(),show=False)
                    plt_state.get_axes()[0].set_xlabel(u"Time (ms)")
                    plt_state.get_axes()[0].set_title("State change: haben > sein")
                    plt_state.savefig(os.path.join("figures","erpcmp_state_{}.pdf".format(filtstr.replace('.',''))))

                    plt_prefix = mne.viz.plot_evoked(prefix_ist,hline=[0],ylim=yscale,show=False)
                    plt_prefix = mne.viz.plot_evoked(prefix_hat,axes=plt_motion.get_axes(),show=False)
                    plt_prefix.get_axes()[0].set_ylabel(u"µV")
                    plt_prefix.get_axes()[0].set_xlabel(u"Time (ms)")
                    plt_prefix.get_axes()[0].set_title("Prefixed state change: haben > sein")
                    plt_prefix.savefig(os.path.join("figures","erpcmp_prefix_{}.pdf".format(filtstr.replace('.',''))))

                    plt_intrans = mne.viz.plot_evoked(intrans_ist,hline=[0],ylim=yscale,show=False)
                    plt_intrans = mne.viz.plot_evoked(intrans_hat,axes=plt_motion.get_axes(),show=False)
                    plt_intrans.get_axes()[0].set_ylabel(u"µV")
                    plt_intrans.get_axes()[0].set_xlabel(u"Time (ms)")
                    plt_intrans.get_axes()[0].set_title("Other intransitive: sein > haben")
                    plt_intrans.savefig(os.path.join("figures","erpcmp_intrans_{}.pdf".format(filtstr.replace('.',''))))

