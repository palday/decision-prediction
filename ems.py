import pylab
import matplotlib.pyplot as plt
import mne
import numpy as np
import glob
import os.path
try:
    import cPickle as pickle
except ImportError:
    import pickle
from mne.decoding import compute_ems

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay")
# kind="standard_1005"
# kind="standard_1020"
# kind="easycap-M10"
montage = mne.channels.read_montage(kind="standard_1020")

event_id = {'A-ist':41,
            'A-hat' :42,
            'B-ist':43,
            'B-hat' :44,
            'C-ist':45,
            'C-hat' :46,
            'D-ist':47,
            'D-hat' :48
            }

epochs = {}
evoked = {}
gats = {}
windows = {
        "N400" : (300, 500),
        "LPC" : (600, 800)
        }

for i in xrange(1,35):
    subj = "{:02d}".format(i)
    print "Subject {}".format(subj)
    raw = mne.io.read_raw_brainvision(os.path.join("raw",subj,subj+".vhdr"),
                                      eog=("H+","H_","V+","V_"),
                                      misc=("A1","A2"),
                                      montage=montage,
                                      preload=True)
    raw.filter(0.3, 30,l_trans_bandwidth=0.15,n_jobs=1,method='iir')
    raw = mne.io.add_reference_channels(raw,"A1")
    #raw = mne.io.set_eeg_reference(raw,["A1","A2"])[0]
    raw = mne.io.set_eeg_reference(raw,None)[0]
    events = raw.get_brainvision_events()
    eventsq = events[:,2]
    rating = np.zeros(len(eventsq),dtype=int)

    # items are not encoded as part of the trigger sequence for this experiment!
    for i,e in enumerate(eventsq):
        if 11 <= e <= 14: # NP
            rating[i] = eventsq[i+5]
        elif 21 <= e <= 28: # aux
            rating[i] = eventsq[i+4]
        elif 31 <= e <= 38: # adverb
            rating[i] = eventsq[i+3]
        elif 41 <= e <= 48: # verb
            rating[i] = eventsq[i+2]

    events[:,1] = rating
    # events without an extracted rating are filler, so we can ignore them
    keep = np.array([bool(x) for x in events[:,1]])
    events = events[keep]

    picks = mne.pick_types(raw.info, eeg=True, eog=True, stim=False, misc=False)
    tmin, tmax = -0.2, 1.0
    baseline = None
    reject = dict(eeg=75e-6, eog=250e-6)

    crit = events[events[:,2] >= 41]
    crit = crit[crit[:,1] != 90]
    crit = crit[crit[:,1] != 109]

    # EMS
    epochs = mne.Epochs(raw, crit,
                        event_id,
                        tmin, tmax,
                        baseline=baseline,
                        preload=True,
                        reject=reject,
                        picks=picks,
                        on_missing='warning')
    # Let's equalize the trial counts in each condition
    epochs.equalize_event_counts(epochs.event_id, copy=False)

    for cond in ('A','B','C','D'):
        # compute surrogate time series
        surrogates, filters, conditions = compute_ems(epochs, [cond+'-ist', cond+'-hat'])

        times = epochs.times * 1e3
        #plt.figure()
        #plt.title('single trial surrogates')
        #plt.imshow(surrogates[conditions.argsort()], origin='lower', aspect='auto',
        #           extent=[times[0], times[-1], 1, len(surrogates)],
        #           cmap='RdBu_r')
        #plt.xlabel('Time (ms)')
        #plt.ylabel('Trials (reordered by condition)')

        plt.figure()
        plt.title('Average EMS signal:' + cond)

        mappings = [(k, v) for k, v in event_id.items() if v in conditions]
        for key, value in mappings:
            ems_ave = surrogates[conditions == value]
            #ems_ave *= 1e13
            plt.plot(times, ems_ave.mean(0), label=key)
        plt.xlabel('Time (ms)')
        plt.ylabel(u'µV/cm')
        plt.legend(loc='best')
        plt.savefig('{}-ems-average.png'.format(cond))

        # visualize spatial filters across time
        #plt.show()
        evoked = epochs.average()
        evoked.data = filters
        evoked.plot_topomap(show=False)
        plt.title(cond)
        plt.savefig('{}-ems-topo.png'.format(cond))

