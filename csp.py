import pylab
import matplotlib.pyplot as plt
import mne
import numpy as np
import glob
import os.path
try:
    import cPickle as pickle
except ImportError:
    import pickle

from sklearn.svm import SVC
from sklearn.cross_validation import ShuffleSplit
from mne.decoding import CSP
from sklearn.pipeline import Pipeline  # noqa
from sklearn.cross_validation import cross_val_score  # noqa

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay")

event_id = {'A-ist':41,
            'A-hat' :42,
            'B-ist':43,
            'B-hat' :44,
            'C-ist':45,
            'C-hat' :46,
            'D-ist':47,
            'D-hat' :48
            }

epochs = {}
evoked = {}
gats = {}
windows = {
        "N400" : (300, 500),
        "LPC" : (600, 800)
        }

for i in xrange(1,2):
    subj = "{:02d}".format(i)
    print "Subject {}".format(subj)
    if subj in gats:
        print "... GAT data already available, skipping"
        continue
    raw = mne.io.read_raw_brainvision(os.path.join("raw",subj,subj+".vhdr"),
                                      eog=("H+","H_","V+","V_"),
                                      misc=("A1","A2"),
                                      preload=True)
    raw.filter(0.3, 30,l_trans_bandwidth=0.15,n_jobs=1,method='iir')
    raw = mne.io.add_reference_channels(raw,"A1")
    raw = mne.io.set_eeg_reference(raw,["A1","A2"])[0]
    events = raw.get_brainvision_events()
    eventsq = events[:,2]
    rating = np.zeros(len(eventsq),dtype=int)

    # items are not encoded as part of the trigger sequence for this experiment!
    for i,e in enumerate(eventsq):
        if 11 <= e <= 14: # NP
            rating[i] = eventsq[i+5]
        elif 21 <= e <= 28: # aux
            rating[i] = eventsq[i+4]
        elif 31 <= e <= 38: # adverb
            rating[i] = eventsq[i+3]
        elif 41 <= e <= 48: # verb
            rating[i] = eventsq[i+2]

    events[:,1] = rating
    # events without an extracted rating are filler, so we can ignore them
    keep = np.array([bool(x) for x in events[:,1]])
    events = events[keep]

    picks = mne.pick_types(raw.info, eeg=True, eog=True, stim=False, misc=False)
    #picks = range(59)
    tmin, tmax = -0.2, 1.2
    baseline = None
    reject = dict(eeg=75e-6, eog=250e-6)

    crit = events[events[:,2] >= 41]
    crit = crit[crit[:,1] != 90]
    crit = crit[crit[:,1] != 109]

    # CSP
    epochs = mne.Epochs(raw, crit,
                        event_id,
                        tmin, tmax,
                        baseline=baseline,
                        preload=True,
                        reject=reject,
                        picks=picks,
                        on_missing='warning')
    labels = epochs.events[:, 1]
    evoked = epochs.average()
    n_components = 3  # pick some components
    svc = SVC(C=1, kernel='linear')
    cv = ShuffleSplit(len(labels), 10, test_size=0.2, random_state=42)
    scores = []
    epochs_data = epochs.get_data()
    csp = CSP(n_components=n_components, reg='lws')
    clf = Pipeline([('CSP', csp), ('SVC', svc)])
    scores = cross_val_score(clf, epochs_data, labels, cv=cv, n_jobs=1)

    print(scores.mean())  # should get better results than above

    # plot CSP patterns estimated on full data for visualization
    csp.fit_transform(epochs_data, labels)
    evoked.data = csp.patterns_.T
    evoked.times = np.arange(evoked.data.shape[0])
    evoked.plot_topomap(times=[0.4,0.5,0.6,0.7,0.8], colorbar=True,size=1.5,layout=layout)

    #pepochs = epochs.pick_channels(['Cz','CPz','Pz'],copy=True)
    #gat = GeneralizationAcrossTime(predict_mode='cross-validation',n_jobs=2)
    #gat.fit(pepochs,y=pepochs.events[:,1])
    #gat.score(pepochs)
    #with open("gat-{}.pickle".format(subj),"wb") as pfile:
    #    pickle.dump(gat,pfile,-1)
