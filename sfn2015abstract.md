---
title: Predicting behavioral preferences in language use from electrophysiological activity
author: Phillip Alday, Matthias Schlesewsky, Ina Bornkessel-Schlesewsky
date: May 2015
---

Traditional ERP research has often divided components into "stimulus-locked" and "response-locked". 
Yet, when we consider that ERP components index evoked electrical activity within the perception-action loop, it becomes clear that a component is not "response-locked" but rather "response-locking". 
Response-related ERPs have been observed in a range of response settings (e.g. the readiness potential and the P300 for positive or neutral settings, the ERN for negative or incorrect responses).
In studies on language, this has been extensively studied with a late positivity for ill-formed sentences, which seems to be strongly dependent on the task (Haupt et al 2008).
Additionally, neural correlates for behavioral preferences without a canonical intersubject answer have been observed in other domains (e.g. beverage: McClure et al 2004, car manufacturer: Schaefer et al 2006), suggesting that it may be possible to measure preferences in language.
(An example of such preferences is the pronunciation of "route" in American English -- it can rhyme with either "root" or "gout" depending on the individual speaker.)
Here, we demonstrate the feasibility of using the electrophysiological response to predict binary judgments in language, thus deriving action from brain activity. 
Moreover, we show that this works in both judgments with and without a canonical answer.

Data were reanalyzed from Roehm et al (2013). 
Sentences were presented phrase-wise using RSVP to 32 monolingually raised native speakers of German (17 women, mean age 24) after they had given informed consent.
After each sentence, subjects performed a binary acceptability judgment as well as a probe task. 
In 3 conditions, the acceptability judgment had a canonical answer, while the fourth lacked one and was subject to individual preference. 

Using generalized linear mixed-effects models with the single-trial acceptability judgment as the dependent variable, we found that the mean single-trial EEG amplitude from centro-parietal sites in the 600-800ms post-stimulus time window was a significant predictor of response across all conditions.
Restricting the analysis to the condition without a canonical judgment reduced the size of the effect but did not remove it.

These results demonstrate the feasibility of using electrophysiological activity to predict behavior, thus situating action in its correct place in the perception-action loop. 
Moreover, they suggest that the difference between "preferred" and "required" response is a quantitative and not a qualitative one.


