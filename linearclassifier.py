import pylab
import matplotlib.pyplot as plt
import mne
import numpy as np
import glob
import os.path
try:
    import cPickle as pickle
except ImportError:
    import pickle
from mne.decoding import GeneralizationAcrossTime

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay")

event_id = {'A-ist':41,
            'A-hat' :42,
            'B-ist':43,
            'B-hat' :44,
            'C-ist':45,
            'C-hat' :46,
            'D-ist':47,
            'D-hat' :48
            }

epochs = {}
evoked = {}
gats = {}
windows = {
        "N400" : (300, 500),
        "LPC" : (600, 800)
        }
