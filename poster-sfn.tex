\documentclass[final]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usetheme{RJHlandscapeUniSA}
\usepackage[orientation=landscape,size=a0,scale=1.4]{beamerposter}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,decorations.markings,positioning}
\tikzset{>=stealth}

\usepackage[absolute,overlay]{textpos}
\usepackage[numbers,compress]{natbib}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{array}
%\usepackage{wrapfig}
\usepackage{rotating}

\graphicspath{{figures/}}

\newcolumntype{C}[1]{>{\centering\arraybackslash} m{#1} }
\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}
\newcommand{\subblock}[1]{\bigskip\textbf{#1}}

\title{Predicting behavioral preferences in language use from electrophysiological activity }
\author{Phillip M. Alday  \and Matthias Schlesewsky \and Ina Bornkessel-Schlesewsky}
\institute{University of South Australia}
\footer{}
%\footer{Live code at \url{https://github.com/jona-sassenhagen/phijona---gamma}}
\date{}

\newcommand{\tikzmark}[3][]{\tikz[overlay,remember picture,baseline] \node [anchor=base,#1](#2) {#3};}
\DeclareMathOperator*{\cooc}{cooc}
\DeclareMathOperator*{\sigcooc}{sig}
% color blind safe colours, see http://www.cookbook-r.com/Graphs/Colors_%28ggplot2%29/#a-colorblind-friendly-palette
\definecolor{lightorange}{HTML}{E69F00}
\definecolor{lightblue}{HTML}{56B4E9}
\definecolor{lightgreen}{HTML}{009E73}
\definecolor{lightyellow}{HTML}{F0E442}
\definecolor{darkblue}{HTML}{0072B2}
\definecolor{darkorange}{HTML}{D55E00}
\definecolor{darkpink}{HTML}{CC79A7}

\begin{document}
\begin{frame}{} 	


\begin{textblock}{55}(1,6)
\begin{block}{Introduction}
Traditional ERP research has often divided components into ``stimulus-locked'' and ``response-locked''. 
Yet, when we consider that ERP components index evoked electrical activity within the perception-action loop, it becomes clear that a component is not ``response-locked'' but rather ``response-locking''. 
Why then do we analyse our data as response-locked and not response-locking?
\end{block}

\begin{block}{Neural correlates of behavioral responses}
Response-related ERPs have been observed in a range of response settings.
In studies on language, this has been extensively studied with a late positivity for ill-formed sentences, which seems to be strongly dependent on the task \cite{hauptschlesewskyroehm2008a}.
Additionally, neural correlates for behavioral preferences without a canonical intersubject answer have been observed in other domains (e.g. beverages \cite{mcclure.li.etal:2004n}, car manufacturers \cite{schaefer.berens.etal:2006n}), suggesting that it may be possible to measure preferences in language.
\end{block}

\begin{block}{To-may-to, To-mah-to --- Acceptability is not absolute}
Variation within a given language permeates all levels -- pronunciation, lexical choice (\emph{biscuit}\slash{}\emph{cookie}), choice of bigram (\emph{different than\slash{}from\slash{}to}) or even verb agreement (\emph{NASA is\slash{}are}).
Such differences may be perceived as anywhere from ``neutral'' to ``dispreferred'' or even ``incorrect'' by other speakers. 
The data presented here are reanalyzed from \cite{roehmsoracebornkessel-schlesewsky2013a} and focus on the selection of the auxiliary verb for different verb classes. 
\end{block}

%\cite{bates.maechler.etal:2015,fox.weisberg:2011,,,roehmsoracebornkessel-schlesewsky2013a,,winklerhaufetangermann2011a}
\begin{block}{From behavioral preferences \dots{}}

\begin{tikzpicture}%[overlay, remember picture,node distance = 3cm]

\node (allconds) [anchor=south west]  {\includegraphics[width=0.4\textwidth]{ratings_by_cond.pdf}};
\node (statebysubj) [node distance = 0.5\textwidth, right of=allconds]{\includegraphics[width=0.4\textwidth]{ratings_by_subj_statechange.pdf}};
%\draw[help lines,xstep=.5,ystep=.5] (0,0) grid (50,15);
%\foreach \x in {0,2,...,50} { \node [anchor=north] at (\x,0) {\x}; }
%\foreach \y in {0,1,...,15} { \node [anchor=east] at (0,\y) {\y}; }
\foreach \y in {2,4.2,6.5,8.8,11.2,13.7} {\draw[->,ultra thick] (19.5,5) to [out=0,in=180] (29,\y); }
\end{tikzpicture}

While subjects were strongly in agreement about the ``correct'' auxiliary in three of the four conditions, the fourth lacked a canonical auxiliary and and the subjects were greatly divided on the acceptability of the two possibilities.

\end{block}

\begin{block}{\dots{} to EEG \dots{}}

\begin{center}
\vspace{-0.4cm}
\small
Differences waves (dispreferred > preferred) for electrodes Cz, CPz, and Pz
\vspace{-0.4cm}
\end{center}
\begin{tikzpicture}%[overlay, remember picture]

\node (motion) [anchor=south west,node distance = 0.23\textwidth]{\includegraphics[width=0.25\textwidth]{erpdiff_motion.pdf}};
\node (prefix) [node distance = 0.23\textwidth, right of=motion]{\includegraphics[width=0.25\textwidth]{erpdiff_prefix.pdf}};
\node (prefix-state) [node distance = 0.12\textwidth,right of=prefix]{};
\node (intrans) [node distance = 0.23\textwidth, right of=prefix]{\includegraphics[width=0.25\textwidth]{erpdiff_intrans.pdf}};
\node (state) [node distance = 0.23\textwidth, right of=intrans]{\includegraphics[width=0.25\textwidth]{erpdiff_state.pdf}};
\node (n400) [above of=prefix-state,node distance=5cm,anchor=south east,align=center]{\raggedleft{} N400};
\node (p600) [below of=prefix-state,node distance=5cm,anchor=north west,align=center]{\raggedleft{} late positivity};
\foreach \x in {7,19,32} {\draw[->,ultra thick] (n400) to [] (\x,7.5); }
\foreach \x in {11,23,35} {\draw[->,ultra thick] (p600) to [] (\x,1.8); }
\node (noeffect) [above of=state,node distance=3cm,anchor=north west,align=center]{\raggedright{} no clear effect};
\draw[red,ultra thick,rounded corners] (43,6.5) rectangle (48,4);
%\draw[help lines,xstep=.5,ystep=.5] (0,0) grid (55,10);
%\foreach \x in {0,2,...,55} { \node [anchor=north] at (\x,0) {\x}; }
%\foreach \y in {0,1,...,8} { \node [anchor=east] at (0,\y) {\y}; }
\end{tikzpicture}

Conditions with a canonical auxiliary show a clear N400-late positivity effect across centro-parietal sites for the non-canonical auxiliary. 
No difference is visible across subjects in the condition without a canonical answer, reflecting a lack of ``consensus'' in the electrophysiology in line with the lack of consensus in the behavioral data. 
\end{block}
\end{textblock}

\begin{textblock}{59}(58,6)
\begin{block}{\ldots{} and back again}
Using generalized linear mixed-effects models (GLMM, \cite{bates.maechler.etal:2015}), we find that single-trial mean amplitude in the time window 600-800ms post stimulus is a significant predictor of behavioral judgements across conditions. Interindividual variance was captured by a random intercept (behavioral biases) and a random slope for amplitude (differences in strength of EEG response).

\begin{center}
\includegraphics[width=0.9\textwidth]{model_trends_all.pdf}

\tiny The colored lines represent per-subject fits, while the black line represents the fixed-effect estimate and its confidence interval for amplitude.
\end{center}
\end{block}

\begin{block}{Preferences are weak judgements}
\begin{tabular}{m{0.45\textwidth} m{0.50\textwidth}}
\includegraphics[width=0.45\textwidth]{model_trends_strength.pdf} & Adding a predictor for ``canonicity'' of expected answer drastically improves model fit. We see that the strength of the response is much stronger for conditions with a canonical answer, but the average dynamics (slope) of the response do not differ strongly between canonical and non-canonical conditions (no significant interaction). There is also far less coherence between subjects for the non-canonical condition and a broadening of the confidence interval.  This reflects a lack of a broad, inter-subject consensus on the correct answer. \\
\end{tabular}
\end{block}

%\begin{block}{Conclusion}
%\begin{tabular}{p{0.70\textwidth}}
% \\
%\end{tabular}
%\end{block}

\begin{block}{Conclusion}
\begin{tabular}{p{0.70\textwidth}}
Perception precedes action in the perception-action loop, and so electrophysiology precedes behavioral response.
Predicting behavioral responses from electrophysiology places action in its correct place in the perception-action loop. 
Here, we have demonstrated that the feasibility of using electrophysiological activity to predict even subtle behavioral preferences.
The difference between ``preferred'' and ``required'' response is a quantitative and not a qualitative one. \\
\end{tabular}
\end{block}

%\begin{block}{EEG predicts ``strong'' judgements}
%\includegraphics[width=0.75\textwidth]{model_trends_can.pdf} 
%\end{block}
%
%\begin{block}{EEG predicts ``weak'' behavioral preferences}
%\includegraphics[width=0.25\textwidth]{model_trends_nocan.pdf}
%\end{block}
\end{textblock}

\begin{textblock}{13.3}(103.7,63.35)
\begin{block}{Literature}
\tiny
\bibliographystyle{poster}
\bibliography{sfn.bib}
\vspace{0.25cm}
\end{block}

\begin{block}{Get a copy!}
\centering
\includegraphics[width=0.43\textwidth]{poster-url.eps}
\end{block}
\end{textblock}

\end{frame}
\end{document}
