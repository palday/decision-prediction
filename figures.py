#!/usr/bin/python
# -*- coding: utf8 -*-

import pylab
import matplotlib.pyplot as plt
import mne
import numpy as np
import os.path
import seaborn as sns

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay")
montage = mne.channels.read_montage(kind="standard_1020")
       
try:
    import cPickle as pickle
except ImportError:
    import pickle
    
with open("cache.pickle","rb") as pfile:
    evoked = pickle.load(pfile)
    evoked_by_cond = pickle.load(pfile)
    grand_average = pickle.load(pfile)

motion  = (grand_average['A-hat'] - grand_average['A-ist']).pick_channels(["Cz","CPz","Pz"],copy=True)
state   = (grand_average['B-hat'] - grand_average['B-ist']).pick_channels(["Cz","CPz","Pz"],copy=True)
prefix  = (grand_average['C-hat'] - grand_average['C-ist']).pick_channels(["Cz","CPz","Pz"],copy=True)
intrans = (grand_average['D-ist'] - grand_average['D-hat']).pick_channels(["Cz","CPz","Pz"],copy=True)

plt_motion =  motion.plot(hline=[0],ylim=dict(eeg=[2, -2]),show=False)
plt_motion.get_axes()[0].set_ylabel(u"µV")
plt_motion.get_axes()[0].set_xlabel(u"Time (ms)")
plt_motion.get_axes()[0].set_title("Motion: haben > sein")
plt_motion.savefig(os.path.join("figures","erpdiff_motion.pdf"), bbox_inches='tight')

plt_state =  state.plot(hline=[0],ylim=dict(eeg=[2, -2]),show=False)
plt_state.get_axes()[0].set_ylabel(u"µV")
plt_state.get_axes()[0].set_xlabel(u"Time (ms)")
plt_state.get_axes()[0].set_title("State change: haben > sein")
plt_state.savefig(os.path.join("figures","erpdiff_state.pdf"), bbox_inches='tight')

plt_prefix =  prefix.plot(hline=[0],ylim=dict(eeg=[2, -2]),show=False)
plt_prefix.get_axes()[0].set_ylabel(u"µV")
plt_prefix.get_axes()[0].set_xlabel(u"Time (ms)")
plt_prefix.get_axes()[0].set_title("Prefixed state change: haben > sein")
plt_prefix.savefig(os.path.join("figures","erpdiff_prefix.pdf"), bbox_inches='tight')

plt_intrans =  intrans.plot(hline=[0],ylim=dict(eeg=[2, -2]),show=False)
plt_intrans.get_axes()[0].set_ylabel(u"µV")
plt_intrans.get_axes()[0].set_xlabel(u"Time (ms)")
plt_intrans.get_axes()[0].set_title("Other intransitive: sein > haben")
plt_intrans.savefig(os.path.join("figures","erpdiff_intrans.pdf"), bbox_inches='tight')


#mne.viz.plot_image_epochs(e['A-hat'].pick_channels(["Cz"],copy=True))
#e = evoked_by_cond['C-sein'][5].pick_channels(["Cz"])
#e = mne.read_epochs(os.path.join("data","01-epo.fif.gz"))

#f, (ax1,ax2) = plt.subplots(nrows=1,ncols=2)
#chan = e['A-hat'].pick_channels(["Cz"],copy=True)
#mne.viz.plot_image_epochs(chan,show=False)
#f.show()
#ax1.set_title("haben")

#ax2.set_title("sein")
#chan = e['A-ist'].pick_channels(["Cz"],copy=True)
#mne.viz.plot_image_epochs(chan,axes=ax2,show=False)
#f.show()
