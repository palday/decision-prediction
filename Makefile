# Copyright (c) 2015, Phillip Alday
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

epochdata = data/01-epo.fif.gz data/01.csv data/02-epo.fif.gz data/02.csv data/03-epo.fif.gz data/03.csv data/04-epo.fif.gz data/04.csv data/05-epo.fif.gz data/05.csv data/06-epo.fif.gz data/06.csv data/07-epo.fif.gz data/07.csv data/08-epo.fif.gz data/08.csv data/09-epo.fif.gz data/09.csv data/10-epo.fif.gz data/10.csv data/11-epo.fif.gz data/11.csv data/12-epo.fif.gz data/12.csv data/13-epo.fif.gz data/13.csv data/14-epo.fif.gz data/14.csv data/15-epo.fif.gz data/15.csv data/16-epo.fif.gz data/16.csv data/17-epo.fif.gz data/17.csv data/18-epo.fif.gz data/18.csv data/19-epo.fif.gz data/19.csv data/20-epo.fif.gz data/20.csv data/21-epo.fif.gz data/21.csv data/22-epo.fif.gz data/22.csv data/23-epo.fif.gz data/23.csv data/24-epo.fif.gz data/24.csv data/25-epo.fif.gz data/25.csv data/26-epo.fif.gz data/26.csv data/27-epo.fif.gz data/27.csv data/28-epo.fif.gz data/28.csv data/29-epo.fif.gz data/29.csv data/30-epo.fif.gz data/30.csv data/31-epo.fif.gz data/31.csv data/32-epo.fif.gz data/32.csv data/33-epo.fif.gz data/33.csv data/34-epo.fif.gz data/34.csv
evokeddata = cache.pickle
pyfigs = figures/erpdiff_intrans.pdf figures/erpdiff_motion.pdf figures/erpdiff_prefix.pdf figures/erpdiff_state.pdf
rfigs = figures/ratings_by_cond.pdf figures/ratings_by_subj_statechange.pdf figures/model_trends_all.pdf figures/model_trends_strength.pdf
filtererps = figures/erpcmp_motion_fft-016_30_200ms_baseline.pdf figures/erpcmp_motion_fft-02_30_200ms_baseline.pdf figures/erpcmp_motion_fft-03_30_200ms_baseline.pdf figures/erpcmp_motion_iir-016_30_200ms_baseline.pdf figures/erpcmp_motion_iir-02_30_200ms_baseline.pdf figures/erpcmp_motion_iir-03_30_200ms_baseline.pdf figures/erpdiff_motion_fft-016_30_200ms_baseline.pdf figures/erpdiff_motion_fft-02_30_200ms_baseline.pdf figures/erpdiff_motion_fft-03_30_200ms_baseline.pdf figures/erpdiff_motion_iir-016_30_200ms_baseline.pdf figures/erpdiff_motion_iir-02_30_200ms_baseline.pdf figures/erpdiff_motion_iir-03_30_200ms_baseline.pdf
filtercache = fft-0.16_30-cache.pickle fft-0.16_30_200ms_baseline-cache.pickle fft-0.2_30-cache.pickle fft-0.2_30_200ms_baseline-cache.pickle fft-0.3_30-cache.pickle fft-0.3_30_200ms_baseline-cache.pickle iir-0.16_30-cache.pickle iir-0.16_30_200ms_baseline-cache.pickle iir-0.2_30-cache.pickle iir-0.2_30_200ms_baseline-cache.pickle iir-0.3_30-cache.pickle iir-0.3_30_200ms_baseline-cache.pickle 

cns-draft: poster-cns.pdf 
	cp poster-cns.pdf poster-cns_`git show -s --format=%ci HEAD | awk '{print $$1}'`_`git rev-parse --short HEAD`.pdf

poster-cns.pdf: $(filtererps) figures/rousselet2012-fig01.pdf cns.bib beamerthemeRJHlandscapeUniSA.sty figures/cnl-color-white.png figures/unisa-landscape.eps figures/poster-cns-url.eps
	latexmk -pdf poster-cns.tex

$(filtererps): filters_figures.py $(filtercache)
	python filters_figures.py
	
$(filtercache): filters.py
	python filters.py
	
cns.pdf : cns.md
	pandoc -o $@ $<

poster-sfn.pdf: poster-sfn.tex beamerthemeRJHlandscapeUniSA.sty sfn.bib figures/cnl-color-white.png figures/unisa-landscape.eps figures/poster-url.eps $(rfigs) $(pyfigs)
	latexmk -pdf poster-sfn.tex

draft: poster-sfn.pdf
	cp poster-sfn.pdf poster-sfn_`git show -s --format=%ci HEAD | awk '{print $$1}'`_`git rev-parse --short HEAD`.pdf

$(rfigs): $(epochdata) sfn.R
	R --vanilla --slave < sfn.R

$(pyfigs): figures.py $(evokeddata)
	python figures.py

$(epochdata) $(evokeddata): erp.py
	mkdir -p data
	python erp.py

figures/poster-cns-url.eps: generateQRcns.py
	python generateQRcns.py
	
figures/poster-url.eps: generateQR.py
	python generateQR.py

clean: 
	rm -f $(pyfigs)
	rm -f $(rfigs)
	rm -f figures/*converted-to.pdf
	rm -f figures/poster-url.eps
	rm -f Rplots.pdf
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.log *.nav *.snm *.toc *.synctex.gz

	
superclean: clean
	rm -rf $(epochdata) 
	rm -rf $(evokedata)
