# -*- coding: utf-8 -*-

import pylab
import matplotlib.pyplot as plt
import mne
import numpy as np
import os.path
#import seaborn as sns
from mne.viz import plot_evoked_topo

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay",scale=True)
layout.pos[:,2] = layout.pos[:,2] * 4
layout.pos[:,3] = layout.pos[:,3] * 5
montage = mne.channels.read_montage(kind="standard_1020")
       
try:
    import cPickle as pickle
except ImportError:
    import pickle
    
with open("cache.pickle","rb") as pfile:
    evoked = pickle.load(pfile)
    evoked_by_cond = pickle.load(pfile)
    grand_average = pickle.load(pfile)

motion_hat = grand_average['A-hat']
motion_hat.comment = "haben (n = 34)"
motion_ist = grand_average['A-ist']
motion_ist.comment = "sein (n = 34)"

state_hat = grand_average['B-hat']
state_hat.comment = "haben (n = 34)"
state_ist = grand_average['B-ist']
state_ist.comment = "sein (n = 34)"


prefix_hat = grand_average['C-hat']
prefix_hat.comment = "haben (n = 34)"
prefix_ist = grand_average['C-ist']
prefix_ist.comment = "sein (n = 34)"

intrans_hat = grand_average['D-hat']
intrans_hat.comment = "haben (n = 34)"
intrans_ist = grand_average['D-ist']
intrans_ist.comment = "sein (n = 34)"

conditions = [motion_hat, motion_ist]
colors = ['red','blue']
v = plot_evoked_topo(conditions,
                 color=colors,
                 title="Verbs of Motion",
                 ylim=dict(eeg=[7,-7]),
                 layout=layout,
#                 fig_facecolor='white',
#                 axis_facecolor='white',
#                 font_color="black",
                 show=False)

for cond, col, pos in zip(conditions, colors, (0.025, 0.07)):
    plt.figtext(0.99, pos, cond.comment, color=col, fontsize=12,
                horizontalalignment='right')

v.savefig("test.pdf")
