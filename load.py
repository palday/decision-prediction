import mne
import matplotlib
#import matplotlib.pyplot as plt
#import seaborn as sns

mne.set_log_level('WARNING')

try:
    import cPickle as pickle
except ImportError:
    import pickle
    
with open("cache.pickle","rb") as pfile:
    evoked = pickle.load(pfile)
    evoked_by_cond = pickle.load(pfile)
    grand_average = pickle.load(pfile)

x = evoked['34']['A-hat']
y = evoked['34']['A-ist']


# basic topo plot; doesn't plot axes or label electrodes
x = grand_average['A-hat']
y = grand_average['A-ist']

motion  = (grand_average['A-hat'] - grand_average['A-ist'])
state   = (grand_average['B-hat'] - grand_average['B-ist'])
prefix  = (grand_average['C-hat'] - grand_average['C-ist'])
intrans = (grand_average['D-ist'] - grand_average['D-hat'])


layout = mne.channels.read_layout("EEG1005.lay",scale=True)
layout.pos[:,2] = layout.pos[:,2] * 4
layout.pos[:,3] = layout.pos[:,3] * 5
v = mne.viz.plot_topo([x,y],
                       layout=layout,
                        ylim=dict(eeg=[5,-5]),
                        fig_facecolor="white",
                        axis_facecolor="white",
                        font_color="black",
                        color=["red","blue"],
                        show=False)
v.savefig("test.pdf")
#for ax in v.get_axes():
#    ax.spines['bottom'].set_color('#dddddd')
#    ax.spines['top'].set_color('#dddddd') 
#    ax.spines['right'].set_color('red')
#    ax.spines['left'].set_color('red')
#    ax.tick_params(axis='x', colors='black')
#    ax.tick_params(axis='y', colors='black')
#    ax.yaxis.label.set_color('black')
#    ax.xaxis.label.set_color('black')
#    ax.title.set_color('black')


 
